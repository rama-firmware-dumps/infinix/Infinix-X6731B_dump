## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 14 UP1A.231005.007 727461 release-keys
- Transsion Name: Infinix ZERO 30
- TranOS Build: X6731B-H896ABC-U-OP-241023V394
- TranOS Version: xos14.5.0
- Brand: INFINIX
- Model: Infinix-X6731B
- Platform: mt6789 (Helio G99)
- Android Build: UP1A.231005.007
- Android Version: 14
- Kernel Version: 5.10.209
- Security Patch: 2024-10-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
- Fingerprint: Infinix/X6731B-OP/Infinix-X6731B:14/UP1A.231005.007/241023V394:user/release-keys
